package com.braisblanco.AmazonWSEchoSystem.s3;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ListObjectsV2Request;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.TransferManagerBuilder;
import com.braisblanco.AmazonWSEchoSystem.dataObject.ChatObject;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class S3Service {

	private static String bucket = "";

	public static boolean bucketPrefixExists(String awsAccessKey, String awsSecretKey, String nickname, String sessionId) {

		AmazonS3 s3 = AmazonS3ClientBuilder.standard().
				withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(awsAccessKey, awsSecretKey)))
				.withRegion(Regions.EU_WEST_3)
				.build();

		ListObjectsV2Request req = new ListObjectsV2Request().withBucketName(bucket).withPrefix(nickname.concat("/").concat(sessionId));

		if (s3.listObjectsV2(req).getKeyCount() == 0) { // Necesitamos crear la clave
			return false;
		}

		return true;

	}

	public static void uploadTextMessage(String awsAccessKey, String awsSecretKey, ChatObject chatObject) throws IOException {

		AmazonS3 s3 = AmazonS3ClientBuilder.standard().
				withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(awsAccessKey, awsSecretKey)))
				.withRegion(Regions.EU_WEST_3)
				.build();
		
		ObjectMapper objectMapper = new ObjectMapper();

		byte[] chatBytes = objectMapper.writeValueAsBytes(chatObject);

		ObjectMetadata omd = new ObjectMetadata();
		omd.setContentLength(chatBytes.length);

		TransferManager transferManager = TransferManagerBuilder.standard().withS3Client(s3).build();
		transferManager.upload(bucket, chatObject.getNickName().concat("/").concat(chatObject.getSessionId()), new ByteArrayInputStream(chatBytes), omd);

	}

	public static ChatObject getChatObject(String awsAccessKey, String awsSecretKey, String nickname, String sessionId) throws JsonParseException, JsonMappingException, IOException {
		
		AmazonS3 s3 = AmazonS3ClientBuilder.standard().
				withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(awsAccessKey, awsSecretKey)))
				.withRegion(Regions.EU_WEST_3)
				.build();
		ObjectMapper objectMapper = new ObjectMapper();
		
		S3Object s3Obj = s3.getObject(bucket, nickname.concat("/").concat(sessionId));
		  
		S3ObjectInputStream byteArray = s3Obj.getObjectContent();
		  
		ChatObject chatObject = objectMapper.readValue(byteArray.readAllBytes(),ChatObject.class);
		  
		return chatObject;
	}
	
	public static ArrayList<String> getChatsWithSearchedText(String awsAccessKey, String awsSecretKey, String nickname, String textToFind) throws JsonParseException, JsonMappingException, IOException {
		AmazonS3 s3 = AmazonS3ClientBuilder.standard().
				withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(awsAccessKey, awsSecretKey)))
				.withRegion(Regions.EU_WEST_3)
				.build();
		ArrayList<String> previousChatsKeys = new ArrayList<>();
		ArrayList<ChatObject> chatsRetrieved = new ArrayList<ChatObject>();
		ArrayList<String> chatsWithSearchedText = new ArrayList<String>();
		
		ListObjectsV2Request req = new ListObjectsV2Request().withBucketName(bucket).withPrefix(nickname.concat("/"));
		
		for (S3ObjectSummary s3ObSum : s3.listObjectsV2(req).getObjectSummaries()) {
			previousChatsKeys.add(s3ObSum.getKey());
		}
		
		ObjectMapper objectMapper = new ObjectMapper();
		
		for(String key: previousChatsKeys) {
		
			S3Object s3Obj = s3.getObject(bucket, key);
		  
			S3ObjectInputStream byteArray = s3Obj.getObjectContent();
		  
			chatsRetrieved.add(objectMapper.readValue(byteArray.readAllBytes(),ChatObject.class));

		}
		
		for(ChatObject chat : chatsRetrieved) {
			for(String text : chat.getChatMessages()) {
				if(text.contains(textToFind)) {
					chatsWithSearchedText.add(text);
				}
			}
		}
		
		return chatsWithSearchedText;
	}
	

}
