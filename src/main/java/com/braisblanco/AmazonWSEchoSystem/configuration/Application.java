package com.braisblanco.AmazonWSEchoSystem.configuration;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;

import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.braisblanco.AmazonWSEchoSystem.queue.QueueListener;
import com.braisblanco.AmazonWSEchoSystem.s3.S3Service;

@SpringBootApplication
@ComponentScan
public class Application {
	
	static Logger logger = LoggerFactory.getLogger(Application.class);
	
	static String queueInboxUrl = "";

	public static void main(String args[]) {
		
		SpringApplication.run(Application.class, args);
		
		QueueListener.listen();
			
	}
	
}
