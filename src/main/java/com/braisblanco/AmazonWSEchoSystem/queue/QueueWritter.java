package com.braisblanco.AmazonWSEchoSystem.queue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.MessageAttributeValue;
import com.amazonaws.services.sqs.model.SendMessageRequest;


public class QueueWritter {
	
	private Logger logger = LoggerFactory.getLogger(QueueWritter.class);
	private String outBoxQueueUrl;
	private AmazonSQS sqs;

	private String awsAccessKey;

	private String awsSecretKey;
	
	public QueueWritter(String outBoxQueueUrl, String accessKey, String secretKey){
		this.outBoxQueueUrl = outBoxQueueUrl;
		this.awsAccessKey = accessKey;
		this.awsSecretKey = secretKey;
	}
	
	public void writeMessageToOutbox(String userNickname, String userSessionId, String message) {
		sqs = AmazonSQSClientBuilder.standard()
				.withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(awsAccessKey, awsSecretKey)))
				.withRegion(Regions.EU_WEST_3)
				.build();
		
		SendMessageRequest sendMessageReq = new SendMessageRequest();
		sendMessageReq.setQueueUrl(outBoxQueueUrl);
		
		MessageAttributeValue attribute = new MessageAttributeValue();
		attribute.setStringValue(userNickname);
		attribute.setDataType("String");

		sendMessageReq.addMessageAttributesEntry("client-id", attribute);
		
		attribute = new MessageAttributeValue();
		attribute.setStringValue(userSessionId);
		attribute.setDataType("String");
		
		sendMessageReq.addMessageAttributesEntry("client-sessionId", attribute);
		
		sendMessageReq.setMessageBody(message);
		sqs.sendMessage(sendMessageReq);
		
		logger.info("Message " + message + " sent to outbox queue.\n");

	}

}
