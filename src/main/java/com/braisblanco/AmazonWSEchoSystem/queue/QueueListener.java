package com.braisblanco.AmazonWSEchoSystem.queue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.DeleteMessageRequest;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.amazonaws.services.sqs.model.ReceiveMessageResult;
import com.braisblanco.AmazonWSEchoSystem.dataObject.ChatObject;
import com.braisblanco.AmazonWSEchoSystem.s3.S3Service;

@Service
public class QueueListener {
	
	private static final String ECHO_MESSAGE_TYPE = "echo";
	
	private static final String SEARCH_MESSAGE_TYPE = "search";
	
	private static final String echoInboxQueueUrl = "";

	private static Logger logger = LoggerFactory.getLogger(QueueListener.class);
	
	private static AmazonSQS sqs;
	
	private static String awsAccessKey = "";
	private static String awsSecretKey = "";
	
		
	public static void listen() {
		
		logger.info("Listening to inbox queue messages.\n");
		
		sqs = AmazonSQSClientBuilder.standard().
				withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(awsAccessKey, awsSecretKey)))
				.withRegion(Regions.EU_WEST_3)
				.build();
			
		while(true) {
			
			ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(echoInboxQueueUrl).withMessageAttributeNames("All").withMaxNumberOfMessages(1).withWaitTimeSeconds(5);
			
			ReceiveMessageResult result = sqs.receiveMessage(receiveMessageRequest);
			
			List<Message> messages = result.getMessages();
					
			if(messages.isEmpty()) {
				logger.info("There are no new messages available.\n");
				continue;
			}
			
			switch(messages.get(0).getMessageAttributes().get("message-type").getStringValue()) {
				
			case ECHO_MESSAGE_TYPE :
				doEcho(messages.get(0));
				break;
			
			case SEARCH_MESSAGE_TYPE:
				doSearch(messages.get(0));
				break;
				
			default:
				break;
			}
			
		}
	}
	
	@PostConstruct
	private static void doEcho(Message message) {
		
		String nickname = "";
		String sessionId = "";
		String messageBody = "";
		
		ChatObject chatObject = new ChatObject();
		ArrayList<String> messagesToUpload = new ArrayList<String>();
		
		QueueWritter queueWritter = new QueueWritter("", awsAccessKey, awsSecretKey);
				
		nickname = message.getMessageAttributes().get("client-id").getStringValue();
		sessionId = message.getMessageAttributes().get("client-sessionId").getStringValue();
		messageBody = message.getBody();
		
		// SENDING MESSAGE TO OUTBOX QUEUE
		logger.info("Message dequeued " + messageBody);
		
		if(S3Service.bucketPrefixExists(awsAccessKey, awsSecretKey, nickname, sessionId)) { 
			try {
				//Retrieve existing conversation data
				chatObject = S3Service.getChatObject(awsAccessKey, awsSecretKey, nickname, sessionId);
			
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
		}else { //Initialize chatObject parameters 
			chatObject.setNickName(nickname);
			chatObject.setSessionId(sessionId);
			chatObject.setChatMessages(new ArrayList<String>());
		}
		
		messagesToUpload = chatObject.getChatMessages();
		messagesToUpload.add(messageBody);
		
		chatObject.setChatMessages(messagesToUpload);
					
		//Store data in S3 bucket
		
		try {
			
			S3Service.uploadTextMessage(awsAccessKey, awsSecretKey, chatObject);
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
									
		queueWritter.writeMessageToOutbox(nickname, sessionId, messageBody);
	
		sqs.deleteMessage(new DeleteMessageRequest(echoInboxQueueUrl, message.getReceiptHandle()));
		
	}
	
	private static void doSearch(Message message) {
		
		QueueWritter queueWritter = new QueueWritter("", awsAccessKey, awsSecretKey);
		
		String nickname = message.getMessageAttributes().get("client-id").getStringValue();
		String sessionId = message.getMessageAttributes().get("client-sessionId").getStringValue();
		String textToFind = message.getBody();
		String messageBody = "";
		ArrayList<String> messagesFound = new ArrayList<String>();
		
		try {
			messagesFound = S3Service.getChatsWithSearchedText(awsAccessKey, awsSecretKey, nickname, textToFind);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		if(messagesFound.size() == 0) {
			queueWritter.writeMessageToOutbox(nickname, sessionId, "There are no messages matching " + textToFind);
		}else {
		
			for(String messageFound : messagesFound){
				messageBody = messageBody.concat(messageFound).concat("\n");
			}
			queueWritter.writeMessageToOutbox(nickname, sessionId, messageBody);
		}
		
		
		sqs.deleteMessage(new DeleteMessageRequest(echoInboxQueueUrl, message.getReceiptHandle()));
		
	}
	
}
