package com.braisblanco.AmazonWSEchoSystem.dataObject;

import java.util.ArrayList;

public class ChatObject {
	
	private String nickName;
	
	private String sessionId;
	
	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	private ArrayList<String> chatMessages;
	
	public ChatObject() {
		
	}
	
	public ChatObject(String nickName, String sessionId, ArrayList<String> chatMessages) {
		this.nickName = nickName;
		this.sessionId = sessionId;
		this.chatMessages = chatMessages; 
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public ArrayList<String> getChatMessages() {
		return chatMessages;
	}

	public void setChatMessages(ArrayList<String> chatMessages) {
		this.chatMessages = chatMessages;
	}
	
	

}
