FROM openjdk:9-jre

COPY target/AmazonWSEchoSystem-0.0.1-SNAPSHOT.jar app/AmazonWSEchoSystem-0.0.1-SNAPSHOT.jar

EXPOSE 8081

CMD ["java", "-jar", "app/AmazonWSEchoSystem-0.0.1-SNAPSHOT.jar"]
